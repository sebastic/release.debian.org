To: debian-devel-announce@lists.debian.org
Subject: Release update: Are we there yet? (infrastructure, no major changes (please), ...)


Hello world,

Since the most recent release update, there have been a couple of
improvements.  First of all, the most important one for most of you is
that thanks to the hard work of Ryan Murray and the other buildd admins,
the infrastructure for testing-security is more or less in place and
ready.  This is however _not_ the announcement of immediate security
support for sarge by the Security Team.  This will be the case after the
freeze, and be announced independently.

(For those interested in the details: all buildds were upgraded to use
ssh3.9 client, and the buildd software was updated (and also deployed to
all buildds) to use the connection caching from ssh3.9, and the
necessary wanna-build databases and buildd chroots have now been
created.)


arm buildd situation
~~~~~~~~~~~~~~~~~~~~
We saw two newer arm buildds being brought online this month, and so the
number of packages waiting for build is constantly going down.  We're
now down to about 200 packages needing build (from more than 600), and
very few of of these are actually needed for the release, as they're new
packages that have never before been built on arm; so we expect
needs-build to be down to a reasonable size for freezing very soon.
Thanks to all the people who helped here, especially to James Troup,
Steve McIntyre, Gerfried Fuchs, Harald Kapper, and Vince Sanders.  (As a
result, we also now have stable-security building again on arm - which
is perhaps not really part of a release update, but many people will be
interested in this news anyway. :)


packages need to build from source by dpkg-buildpackage
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
In the past, there have been some kernel modules that were not built via
a "normal" dpkg-buildpackage, but more or less by hand from the kernel
images.  The agreement reached by the release team, the ftp team and the
security team is that we only want packages that can be rebuilt with
dpkg-buildpackage, as this is necessary for security bug fixes.  Also, our
release policy says that "Packages must autobuild without failure" which
includes that they are built using debian/rules, and create all the binary
packages that are actually in the archive.


amd64
~~~~~
As already known, we're too near to release to add amd64 to our official
archive.  However, the porters have decided to make an unofficial
release of amd64 that is synchronized to sarge, and the porters will
provide security support for that release.  We appreciate this.  Adding
amd64 to the main archive will happen as planned after the release of
sarge.


no major new changes / upload targets
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
So, we're getting nearer to the release.  We will get there more quickly
if disruptive changes in unstable are kept to a minimum, so please don't
upload major new changes to unstable right now.  Moreover, when we said
"last call" on April 1, we meant it:  we thought we would get here more
quickly than we did, but that's all the more reason to not give more
time for last-minute changes.  At this point, assume that any
low-priority changes you upload to unstable will *not* make it into
sarge.

Please also use the following guidelines, which will remain more or less
unchanged during the freeze, when deciding which suite to upload to:

  - If the changes are meant post-sarge, please do not upload to unstable,
    but to experimental.

  - If your package is frozen, but the version in unstable contains no
    changes unsuitable for testing, upload to unstable only and contact
    debian-release@lists.debian.org if changes need to be pushed through
    to testing.

  - If your package is frozen and the version in unstable includes
    changes that should NOT be part of sarge, contact
    debian-release@lists.debian.org with details about the changes you
    plan to upload (diff -u preferred) and, with approval, upload to
    testing-proposed-updates.  Changes should be limited to translation
    updates and fixes for important or RC bugs.

  - If you need to do a bug fix directly in sarge, you will need to
    upload to testing-proposed-updates as well, with the same
    requirements as for frozen packages.

  - All other package updates should be uploaded to unstable only.
    Please use urgency=high for uploads that fix RC bugs for sarge
    unless you believe there is a significant chance of breakage in the
    new package.

If you want to increase the urgency of an already uploaded package,
please speak with the release team; this can be sorted out without the
need for another upload.


Cheers,
-- 
Andi Barth
Debian Release Team
http://release.debian.org/
