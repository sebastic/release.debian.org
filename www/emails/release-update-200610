DRAFT!

Subject: release adjustments

Hi,

as you (probably) all noticed, we delayed the full final freeze of Etch a
bit. This mail contains important information what you should do as
maintainer of any frozen package (which will be all packages soon), please
read at least this part. Also, please don't upload changes that are not
meant for Etch to unstable. You can of course use experimental for any
changes, however experimental they are. Also, please don't do any shlib
bumps anymore, unless coordinated with the release team. And of course, the
time for transitions prior Etch is history.


Frozen Packages
===============
If you are maintainer of a frozen package, please upload only important,
serious, grave and critical bugfixes and localisation or documentation
changes.  Please also inform the release team about any such changes, so
that we can review your package and approve it for Etch.

In cases of doubt, please speak with the release team before uploading - we
definitly prefer questions asked too much over uploads not suitable for
Etch.


Kernel Status
=============
With the General Resolution about kernel firmware issues, the release team
considers the kernel situation cleared up enough now for release. Please
see our mail to the kernel team [1] for more information.


Release Critical Bug Status
===========================
Debian saw lots of Bug Squashing Parties recently - thanks to all
participiants. We made great progress, and are slowly getting back on track
to release in December.

The number of release critical bugs went down a bit to 240 with that - that
the count went up to about 310 is because we track bugs better now (please
see below). The number of 240 is better than it sounds, as many
long-standing bugs are fixed now - less than 80 bugs are older than 14
days, and about 30 of such bugs are already fixed in unstable. We still
need some time to go for the full freeze, as we want to avoid the release
team needing to spend more time on reviewing patches then on actually
fixing bugs.

A long-awaited change is that version tracking has now fully be switched on
on ftp-master, and so bugs fixed in Non-Maintainer Uploads will be properly
marked as done with the correct version. Also, most of the previously
marked "fixed" bugs were reopened and closed correctly. Though this
increased the bug count by about 70 bugs waiting for transition, this will
help us to not miss the transition of any such fix to Etch. For Sarge, we
had to spend major effort quite late in the release cycle on that, for
Etch, this is now easier.


Release Policy
==============
We have done some last minute adjustments on the release policy. We have
now written down that Etch will be LSB 3.1-compatible - one could actually
expect that for some time now. Another formal change is that we consider
kernel patches that don't apply to the current 2.6-kernels as RC-buggy
another thing we're doing for some time now. We don't consider the
non existence of the required targets in debian/rules as RC, as long as the 
buildds don't have issues - perhaps we want to change that post-Etch though. 
And, the python policy changed a bit in the last half year, so even without
writing it down, that release policy statement isn't current anymore.

As the last two months prior to the release have started, we have started
to treat issues reported late as RC-buggy only for Etch+1 - the same we did
with Sarge. For example, back at that time we got a report in the very last
minute that sendmail had a serious upgrade issue (but without real data
loss). As the release was almost done, we decided to "just" document it
properly in the release notes. We are not yet at that state with Etch, but
as release comes nearer and nearer, we will start to consider more and more
issues reported late as "sorry, reported to late". Of course, real data
loss issues will always be considered RC, as well as some other vital
issues. So, if you know problems, please report them *now*. And, please
still feel free to fix etch-ignored issues, and ask the release team for a
freeze exception/special treatment if the package doesn't make it to Etch
by itself (such issues won't show up on our radar by themself).





So much for now. Thanks to those pulling towards Etch for your steady
work. Let's work together, and let's make a good, stable, high-quality
release of Etch in December.



Cheers,
Andi
-- 
Debian Release Team
http://release.debian.org/
