Subject: release update: Release goals, testing transition, arch requalification
To: debian-devel-announce@lists.debian.org

Hi,

We have promised to send out regular release updates, and this one is
supposed to be the first one in a long tradition.  In the future, our
release updates will also contain bits about important upcoming changes
to unstable and testing - if you want to have something included in the
release update, please mail debian-release@lists.debian.org or ping one of us
in private.


Release goals
~~~~~~~~~~~~~
In our last mail, we asked for release goal proposals and said we would
decide about those soonish. We received a lot of proposals, and in speaking
about them, we got experience how a goal should look like to be acceptable.
Of course, we're still learning more about them, so please consider this
to be guidelines.

For a proposal to be eligible as release goal, it needs to be more or less
in a state that it is agreed by (at least most) of the relevant developers.
For example, if you want a new python policy (what we had for Etch as
release goal), it needs basically support by the python-packages maintainer
plus the debian-python list. In case it includes non-trivial policy changes
or technical bits, we usually require some technical document that
describes what needs to be done and why; this is not required to be already
part of the formal policy, but of course a bug against policy is always a
good idea.  In other words, "release goal" is a shortcut for *implementing*
changes, it is not a short cut for *discussing* changes (and please
remember, debian-release is not a discussion list anyways. If submission of
a release goal reveals that there needs to be a proper discussion, there
are better places.

As for now, we track all release goals in
http://release.debian.org/lenny-goals.txt - if you miss your favourite
goal, please remember someone from the release team to include missing
goals.


Ok, now to the approved release goals:
- full IPv6 support
- double compilation support
  All packages should be able to be built twice in a row.
- No unmet recommends relations inside main
- Drop debmake from Debian
- i18n support in all debconf-using packages
- full large file support
- NFS version 4 support
- Dependency/File list predictability
- I18n support for package descriptions
- all debian/changelog and debian/control are in UTF-8
  Please remember that an ascii-only file is already in utf-8.
- all /bin/sh-scripts are dash-compatible (but of course need to stay
  bash-compatible as well)
- prepare init.d-scripts for dependency-based init


Testing transition
~~~~~~~~~~~~~~~~~~
The mess created by the simultaneous upload of new libraries after the
etch release has been mostly cleared, the testing transition should now
work smoothly in most cases. If you notice any problems, don't hesitate
to prod us.

However, as always, there is already a new issue on our radar: We need to
get glib1.2 and glib2.0 to testing which requires quite many packages to be
available at the same time.


Upcoming changes
~~~~~~~~~~~~~~~~
* KDE4
  The QT/KDE team has started to package the alpha releases of KDE4. This
  work will be included in experimental as soon as upstream has decided
  on the final module structure for the new KDE major releases.

* Gnome 2.20
  The first bits of the next Gnome release, 2.20, have been uploaded to
  experimental. This includes the new versions of GLib, Pango, ATK and
  GTK+, which bring with them some of the consolidation and integration
  work done on the Gnome desktop. GLib now provides a regular expression
  interface (GRegex) and Gtk+ has integrated a Glade-compatible interface
  builder (Gtk-Builder). Please note that these experimental packages
  have not frozen their API yet.

* Toolchain
  Glibc will stop working on sparc v8 machines, and check prior to
  installation whether someone tries to install it on such a machine. After
  that change (and after the glib transition happend), gcc will be switched
  to generate sparc v9-code. Also, the default gcc compiler will be gcc 4.2.

* Xorg 7.3
  The Debian X Strike Force is going to package one of the next alpha/beta
  releases of Xorg 7.3 to experimental. The current version in unstable 
  should already mostly work without a configuration file (See [X1] for 
  more information about the current version) and output hotplugging is
  coming. An up-to-date status of the plans for Lenny can be found at [X2].
  The team is still looking for people with the right hardware to work on 
  avivo (ATI R500 cards) and nouveau (nVidia cards).

  [X1] http://wiki.debian.org/XStrikeForce/ReleaseNotes
  [X2] http://wiki.debian.org/XStrikeForce/XSFTODO


Release architecture requalification
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
To make sure we deliver quality for all the architectures we support, we
want to start the architecture requalification process from scratch again.
Therefor we invite people to create and fill in the respective wiki pages. 
Please only add yourself to the wiki page when you are an active developer
on the architecture. A wiki template can be found at [2], please name the
wiki page <arch>LennyReleaseRecertification.



Cheers,
Luk Claes

Footnotes:
[1]   http://lists.debian.org/debian-devel-announce/2007/06/msg00007.html
[2]   http://wiki.debian.org/LennyReleaseRecertificationTemplate
